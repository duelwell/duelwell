﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpperCollisionScript : MonoBehaviour
{
    public static int upperLayer = 8; //groundLayer
    public static String otherName;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Upper trigger entry by : " + other.name + " object.");
        var o = other.gameObject;
        upperLayer = o.layer;

        otherName = o.name;
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Upper trigger exit, reset to ground");
        upperLayer = 8; //groundLayer
    }
}
